let keyStates = new Map();

document.addEventListener('keydown', (event) => {
	if(event.code == 'ArrowUp' && !keyStates.get(event.code)) {
		move('forward')
	} else if(event.code == 'ArrowDown' && !keyStates.get(event.code)) {
		move('backward')
	} else if(event.code == 'ArrowLeft' && !keyStates.get(event.code)) {
		move('left')
	} else if(event.code == 'ArrowRight' && !keyStates.get(event.code)) {
		move('right')
	}
	keyStates.set(event.code, true)
}, false);

document.addEventListener('keyup', (event) => {
	keyStates.set(event.code, false)
	if(!keyStates.get('ArrowUp') && !keyStates.get('ArrowDown')) {
		move('stop')
	}
	if(!keyStates.get('ArrowLeft') && !keyStates.get('ArrowRight')) {
		move('keep')
	}
}, false);


function move(direction) {
	httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		return false;
	}
	httpRequest.onreadystatechange = onReadyStateChange;
	httpRequest.open('GET', '/move/'+direction);
	httpRequest.send();
}

function onReadyStateChange() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			console.log(httpRequest.responseText)
		} else {
			console.log(httpRequest.responseText)
		}
	}
}