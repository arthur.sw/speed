let keyStates = new Map();

document.addEventListener('keydown', (event) => {
	if(event.code == 'ArrowUp' && !keyStates.get(event.code)) {
		move('forward')
	} else if(event.code == 'ArrowDown' && !keyStates.get(event.code)) {
		move('backward')
	} else if(event.code == 'ArrowLeft' && !keyStates.get(event.code)) {
		move('left')
	} else if(event.code == 'ArrowRight' && !keyStates.get(event.code)) {
		move('right')
	}
	keyStates.set(event.code, true)
}, false);

document.addEventListener('keyup', (event) => {
	keyStates.set(event.code, false)
	if(!keyStates.get('ArrowUp') && !keyStates.get('ArrowDown')) {
		move('stop')
	}
	if(!keyStates.get('ArrowLeft') && !keyStates.get('ArrowRight')) {
		move('keep')
	}
}, false);

// function move(direction) {
// 	httpRequest = new XMLHttpRequest();

// 	if (!httpRequest) {
// 		return false;
// 	}
// 	httpRequest.onreadystatechange = onReadyStateChange;
// 	httpRequest.open('GET', '/move/'+direction);
// 	httpRequest.send();
// }

// function onReadyStateChange() {
// 	if (httpRequest.readyState === XMLHttpRequest.DONE) {
// 		if (httpRequest.status === 200) {
// 			console.log(httpRequest.responseText)
// 		} else {
// 			console.log(httpRequest.responseText)
// 		}
// 	}
// }

const PORT = 3030
let socket = new WebSocket('ws://' + location.hostname + ':' + PORT)

socket.addEventListener('message',  (event)=> onMessage(event))
socket.addEventListener('open',  (event)=> onWebSocketOpen(event))
socket.addEventListener('close',  (event)=> onWebSocketClose(event))
socket.addEventListener('error',  (event)=> onWebSocketError(event))

function onMessage(event) {
	// let json = JSON.parse(event.data);
	console.log(event.data)
}
function onWebSocketOpen(event) {
	console.log('open')
	console.log(event)
}

function onWebSocketClose(event) {
	console.log('close')
	console.log(event)
}

function onWebSocketError(event) {
	console.error('WebSocket error')
	console.error(event)
}

function move(direction) {
	console.log(direction)
	socket.send(direction)
}


document.addEventListener("DOMContentLoaded", function(event) {
    video = document.getElementById('video')
	video.setAttribute('src', 'http://' + location.hostname + ':8080/stream/video.mjpeg')
});