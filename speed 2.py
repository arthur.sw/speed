from flask import Flask, render_template
from multiprocessing import Process, Value
from time import sleep

import RPi.GPIO as io
import wiringpi
 
# use 'GPIO naming'
wiringpi.wiringPiSetupGpio()

ServoPin = 18

io.setmode(io.BCM)
 
# set ServoPin to be a PWM output
wiringpi.pinMode(ServoPin, wiringpi.GPIO.PWM_OUTPUT)

# set the PWM mode to milliseconds stype
wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)

wiringpi.pwmWrite(ServoPin, currentServoPosition)

# Motors
in1_pin = 4 	# In1 Pin
in2_pin = 17	# In2 Pin

io.setup(in1_pin, io.OUT)
io.setup(in2_pin, io.OUT)

def set(property, value):
	try:
		f = open("/sys/class/rpi-pwm/pwm0/" + property, 'w')
		f.write(value)
		f.close()	
	except:
		print("Error writing to: " + property + " value: " + value)
 
set("delayed", "0")
set("mode", "pwm")
set("frequency", "500")
set("active", "1")

def stop():
	io.output(in1_pin, False)    
	io.output(in2_pin, False)
	print('stop')

def clockwise():
	io.output(in1_pin, True)    
	io.output(in2_pin, False)
	print('clockwise')
 
def counter_clockwise():
	io.output(in1_pin, False)
	io.output(in2_pin, True)
	print('counter_clockwise')

app = Flask(__name__)

servoCommand = Value('i', 0)
motorCommand = Value('i', 0)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/move/<direction>')
def move(direction):
	print('move: ' + direction)
	if direction == 'forward':
		motorCommand.value = 1
	elif direction == 'backward':
		motorCommand.value = -1
	elif direction == 'stop':
		motorCommand.value = 0
	elif direction == 'left':
		servoCommand.value = -1
	elif direction == 'right':
		servoCommand.value = 1
	elif direction == 'keep':
		servoCommand.value = 0
	return ''

def hardware_loop(servoCommand, motorCommand):
	currentServoPosition = 0
	servoSpeed = 1
	motorSpeed = 1
	motorAcceleration = 1
	while True:
		currentServoPosition += servoCommand.value * servoSpeed
		wiringpi.pwmWrite(ServoPin, currentServoPosition)

		if motorCommand.value == 1:
			clockwise()
		elif motorCommand.value == -1:
			counter_clockwise()
		elif motorCommand.value == 0:
			stop()

		motorSpeed += motorCommand.value * motorAcceleration
		print('motorSpeed: ' + str(motorSpeed))
		print('currentServoPosition: ' + str(currentServoPosition))
		print('servoCommand: ' + str(servoCommand.value))
		print('motorCommand: ' + str(motorCommand.value))
		set("duty", str(motorSpeed))

		sleep(0.01)
		# sleep(0.5)


if __name__ == "__main__":
	p = Process(target=hardware_loop, args=(servoCommand, motorCommand,))
	p.start()  
	app.run(debug=True, use_reloader=False)
	p.join()
