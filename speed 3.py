from flask import Flask, render_template
from multiprocessing import Process, Value
from time import sleep

import RPi.GPIO as GPIO

minAngle = -1
maxAngle = 1
minSpeed = 0
maxSpeed = 100
dutyCycleMin = 7
dutyCycleMax = 10

ServoPin = 32

MotorPin1 = 3
MotorPin2 = 5
MotorEnable = 33

GPIO.setmode(GPIO.BOARD)

GPIO.setup(ServoPin, GPIO.OUT)


GPIO.setup(MotorPin1, GPIO.OUT)
GPIO.setup(MotorPin2, GPIO.OUT)
GPIO.setup(MotorEnable, GPIO.OUT)

servoPWM = GPIO.PWM(servoPIN, 50) 	# 50 Hz
servoPWM.start(2.5)				 	# Duty cycle (0.0 <= dc <= 100.0)

motorPWM = GPIO.PWM(MotorEnable, 100)
motorPWM.start(0)

# Servo expect a duty cycle between 5% and 10% of the period
def angleToDutyCycle(angle):
	return dutyCycleMin + (dutyCycleMax - dutyCycleMin) * (angle + 1.0) / 2.0

def setServoAngle(angle):
	servoPWM.ChangeDutyCycle(angleToDutyCycle(angle))

def stop():
	GPIO.output(MotorPin1, GPIO.LOW)
	GPIO.output(MotorPin2, GPIO.LOW)
	print('stop')

def clockwise():
	GPIO.output(MotorPin1, GPIO.HIGH)
	GPIO.output(MotorPin2, GPIO.LOW)
	print('clockwise')
 
def counter_clockwise():
	GPIO.output(MotorPin1, GPIO.LOW)
	GPIO.output(MotorPin2, GPIO.HIGH)
	print('counter_clockwise')

app = Flask(__name__)

servoCommand = Value('i', 0)
motorCommand = Value('i', 0)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/move/<direction>')
def move(direction):
	print('move: ' + direction)
	if direction == 'forward':
		motorCommand.value = 1
	elif direction == 'backward':
		motorCommand.value = -1
	elif direction == 'stop':
		motorCommand.value = 0
	elif direction == 'left':
		servoCommand.value = -1
	elif direction == 'right':
		servoCommand.value = 1
	elif direction == 'keep':
		servoCommand.value = 0
	elif direction == 'shutdown':
		servoPWM.stop()
		motorPWM.stop()
		GPIO.cleanup()
	return ''

def hardware_loop(servoCommand, motorCommand):
	servoPosition = 0
	servoSpeed = 1
	motorSpeed = 1
	motorAcceleration = 1
	while True:
		servoPosition += servoCommand.value * servoSpeed

		servoPosition = max(minAngle, min(maxAngle, servoPosition))

		setServoAngle(servoPosition)

		if motorCommand.value == 1:
			clockwise()
		elif motorCommand.value == -1:
			counter_clockwise()
		elif motorCommand.value == 0:
			stop()

		motorSpeed += motorCommand.value * motorAcceleration
		
		motorSpeed = max(minSpeed, min(maxSpeed, motorSpeed))

		print('motorSpeed: ' + str(motorSpeed))
		print('servoPosition: ' + str(servoPosition))
		print('servoCommand: ' + str(servoCommand.value))
		print('motorCommand: ' + str(motorCommand.value))
		
		motorPWM.ChangeDutyCycle(motorSpeed)

		sleep(0.01)
		# sleep(0.5)


if __name__ == "__main__":
	p = Process(target=hardware_loop, args=(servoCommand, motorCommand,))
	p.start()  
	app.run(debug=True, use_reloader=False)
	p.join()

