from flask import Flask, render_template
# from multiprocessing import Process, Value
from time import sleep
import threading


import RPi.GPIO as GPIO

minAngle = -1
maxAngle = 1
minSpeed = 0
maxSpeed = 100
dutyCycleMin = 7
dutyCycleMax = 10

ServoPin = 11

MotorPin1 = 16
MotorPin2 = 18
MotorEnable = 22

GPIO.setmode(GPIO.BOARD)

GPIO.setup(ServoPin, GPIO.OUT)


GPIO.setup(MotorPin1, GPIO.OUT)
GPIO.setup(MotorPin2, GPIO.OUT)
GPIO.setup(MotorEnable, GPIO.OUT)

servoPWM = GPIO.PWM(ServoPin, 50) 	# 50 Hz
servoPWM.start(2.5)				 	# Duty cycle (0.0 <= dc <= 100.0)

motorPWM = GPIO.PWM(MotorEnable, 100)
motorPWM.start(50)

sleep(1)

motorPWM.ChangeDutyCycle(100)

sleep(1)

# Servo expect a duty cycle between 5% and 10% of the period
def angleToDutyCycle(angle):
	return dutyCycleMin + (dutyCycleMax - dutyCycleMin) * (angle + 1.0) / 2.0

def setServoAngle(angle):
	servoPWM.ChangeDutyCycle(angleToDutyCycle(angle))

def stop():
	GPIO.output(MotorPin1, GPIO.LOW)
	GPIO.output(MotorPin2, GPIO.LOW)
	print('stop')

def clockwise():
	GPIO.output(MotorPin1, GPIO.HIGH)
	GPIO.output(MotorPin2, GPIO.LOW)
	print('clockwise')
 
def counter_clockwise():
	GPIO.output(MotorPin1, GPIO.LOW)
	GPIO.output(MotorPin2, GPIO.HIGH)
	print('counter_clockwise')

app = Flask(__name__)

motorCommand = 0
servoCommand = 0

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/move/<direction>')
def move(direction):
	global motorCommand
	global servoCommand
	print('move: ' + direction)
	if direction == 'forward':
		motorCommand = 1
	elif direction == 'backward':
		motorCommand = -1
	elif direction == 'stop':
		motorCommand = 0
	elif direction == 'left':
		servoCommand = -1
	elif direction == 'right':
		servoCommand = 1
	elif direction == 'keep':
		servoCommand = 0
	elif direction == 'shutdown':
		servoPWM.stop()
		motorPWM.stop()
		GPIO.cleanup()

	# print('move: motorCommand: ' + str(motorCommand))
	# print('move: servoCommand: ' + str(servoCommand))
	return ''

servoPosition = 0
servoPositionOffsetMax = 0.3
lastServoPosition = 0
servoSpeed = 0.1
motorSpeed = 10
motorAcceleration = 1

n = 0
def hardware_loop():
	global servoPosition
	global servoSpeed
	global motorSpeed
	global motorAcceleration
	global servoPositionOffsetMax
	global lastServoPosition
	global motorPWM
	global motorCommand
	global servoCommand
	global n

	# if n > 10:
	# 	n = 0
	# 	print('motorCommand: ' + str(motorCommand))
	# 	print('servoCommand: ' + str(servoCommand))
	# 	print('motorSpeed: ' + str(motorSpeed))
	# 	print('servoPosition: ' + str(servoPosition))
	# n += 1

	servoPosition += servoCommand * servoSpeed

	servoPosition = max(minAngle, min(maxAngle, servoPosition))

	if abs(servoPosition - lastServoPosition) > servoPositionOffsetMax:
		lastServoPosition = servoPosition
		setServoAngle(servoPosition)

	if motorCommand == 1:
		clockwise()
		motorSpeed += motorAcceleration
	elif motorCommand == -1:
		counter_clockwise()
		motorSpeed += motorAcceleration
	else:
		motorSpeed -= motorAcceleration

	# motorSpeed += abs(motorCommand) * motorAcceleration
	
	motorSpeed = max(minSpeed, min(maxSpeed, motorSpeed))

	motorPWM.ChangeDutyCycle(motorSpeed)
	
	t = threading.Timer(0.1, hardware_loop)
	t.start()

hardware_loop()

if __name__ == "__main__":
	app.run(debug=True)

