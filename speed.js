const DEBUG = false

const WebSocket = require('ws');


class GpioDebug {

	constructor() {

	}

	pwmFrequency() {

	}	

	pwmWrite() {

	}

	digitalWrite() {

	}
}

const Gpio = !DEBUG ? require('pigpio').Gpio : GpioDebug

if(!DEBUG) {
	for (let gpioNo = Gpio.MIN_GPIO; gpioNo <= Gpio.MAX_GPIO; gpioNo += 1) {
		const gpio = new Gpio(gpioNo);

		console.log('GPIO ' + gpioNo + ':' +
		' mode=' + gpio.getMode() +
		' level=' + gpio.digitalRead()
		);
	}
}

let minAngle = -1
let maxAngle = 1
let minSpeed = 0
let maxSpeed = 100
let dutyCycleMin = 5
let dutyCycleMax = 12

// // BOARD
// let ServoPin = 11 

// let MotorPin1 = 16
// let MotorPin2 = 18
// let MotorEnable = 22

// BCM
let ServoPin = 17

let MotorPin1 = 23
let MotorPin2 = 24
let MotorEnable = 25

const LOW = 0
const HIGH = 1

let motorCommand = 0
let servoCommand = 0

const PORT = 3030

const wss = new WebSocket.Server({ port: PORT });

wss.on('connection', function connection(ws) {

	console.log('connection opened')

  	ws.on('message', function incoming(message) {
    	console.log('received: %s', message);
		let direction = message

		if(direction == 'forward') {
			motorCommand = 1
		} else if (direction == 'backward') {
			motorCommand = -1
		} else if(direction == 'stop') {
			motorCommand = 0
		} else if(direction == 'left') {
			servoCommand = -1
		} else if(direction == 'right') {
			servoCommand = 1
		} else if(direction == 'keep') {
			servoCommand = 0
		} else if(direction == 'shutdown') {
			// servoPWM.stop()
			// motorPWM.stop()
			// GPIO.cleanup()
		}
  	});

  	ws.send('welcome');
});


servoPWM = new Gpio(ServoPin, {mode: Gpio.OUTPUT})

if(!DEBUG) {
	console.log("==== PIN stat ====");
	console.log("servoPWM range: " + servoPWM.getPwmRange());
	console.log("servoPWM freq: " + servoPWM.getPwmFrequency());
}



servoPWM.pwmFrequency(50)
servoPWM.pwmWrite(Math.round(255 * 2.5 / 100))

motorPWM = new Gpio(MotorEnable, {mode: Gpio.OUTPUT})

if(!DEBUG) {
	console.log("==== PIN stat ====");
	console.log("motorPWM range: " + motorPWM.getPwmRange());
	console.log("motorPWM freq: " + motorPWM.getPwmFrequency());
}

// motorPWM.pwmFrequency(100)
// motorPWM.pwmWrite(0)


let motor1 = new Gpio(MotorPin1, {mode: Gpio.OUTPUT})
let motor2 = new Gpio(MotorPin2, {mode: Gpio.OUTPUT})

// Servo expect a duty cycle between 5% and 10% of the period
let angleToDutyCycle = (angle)=> {
	return dutyCycleMin + (dutyCycleMax - dutyCycleMin) * (angle + 1.0) / 2.0
}

let setServoAngle = (angle)=> {
	servoPWM.pwmWrite(Math.round(255 * angleToDutyCycle(angle) / 100))
}

let stop = ()=> {
	motor1.digitalWrite(LOW)
	motor2.digitalWrite(LOW)
}

let clockwise = ()=> {
	motor1.digitalWrite(HIGH)
	motor2.digitalWrite(LOW)
}

let counter_clockwise = ()=> {
	motor1.digitalWrite(LOW)
	motor2.digitalWrite(HIGH)
}


servoPosition = 0
servoPositionOffsetMax = 0.3
lastServoPosition = 0
servoSpeed = 0.1
motorSpeed = 0
motorAcceleration = 10

n = 0

let hardware_loop = ()=> {
	// if n > 10:
	// 	n = 0
	// 	print('motorCommand: ' + str(motorCommand))
	// 	print('servoCommand: ' + str(servoCommand))
	// 	print('motorSpeed: ' + str(motorSpeed))
	// 	print('servoPosition: ' + str(servoPosition))
	// n += 1

	// console.log('motorCommand: ' + motorCommand)
	// console.log('servoCommand: ' + servoCommand)
	// console.log('motorSpeed: ' + motorSpeed)
	// console.log('servoPosition: ' + servoPosition)

	servoPosition += servoCommand * servoSpeed

	servoPosition = Math.max(minAngle, Math.min(maxAngle, servoPosition))

	if(Math.abs(servoPosition - lastServoPosition) > servoPositionOffsetMax) {
		lastServoPosition = servoPosition
		setServoAngle(servoPosition)
	}

	if(motorCommand == 1){
		clockwise()
		motorSpeed += motorAcceleration
		motorPWM.digitalWrite(HIGH)
	} else if (motorCommand == -1) {
		counter_clockwise()
		motorSpeed += motorAcceleration
		motorPWM.digitalWrite(HIGH)
	} else {
		motorSpeed -= motorAcceleration
		motorPWM.digitalWrite(LOW)
	}
		
	// # motorSpeed += Math.abs(motorCommand) * motorAcceleration
	
	motorSpeed = Math.max(minSpeed, Math.min(maxSpeed, motorSpeed))

	// motorPWM.pwmWrite(Math.round(255 * motorSpeed / 100))

}

setInterval(hardware_loop, 10)
	